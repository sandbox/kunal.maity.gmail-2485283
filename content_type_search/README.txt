Content Type Search: The Drupal 7 Content Type Search module
--------------------------------------------------------------------

This module allows you to search content type in node add(node/add) page.

This version of the module only works with Drupal 7.x.

Suppose you are an authenticated content creator or administrator. 
Now for your web application you have more than 50+ content types. 
Now in time of adding a content or node page in a particular content 
type you have to first find out that content type, which may become 
a annoying task. Now with this search module you will get your 
expected or exact content type very easily.

Features
------------------------------------------------------------------------------
The primary features include:

* Admin end search in node/add page to search content type in which we 
  have to add content.

* Have auto-fill as well as filtered list facility

* User can directly go to node add page by clicking on desire searched 
  content type

Installing content type search module (first time installation)
------------------------------------------------------------------------------
1. Copy the module as normal.

2. Enable the "Content Type search Enhancement" module from the module 
   administration page (Administer >> Site configuration >> Modules).

3. Configure the module from admin configuration section.

Credits / Contact
------------------------------------------------------------------------------
The current maintainer is Kunal Maity.

Author
-------------------------------------------------------
Kunal Maity.
