/**
 * @file
 * JS file to support some operation of content type search module.
 */

function filter_list_result() {
  var str = document.getElementById("edit-text-to-filter").value;
  if (str.length >= 3) {
    var res = jstring.split("`");
    var len = res.length;
    var html = '<thead><tr><th>Content Type</th></tr></thead><tbody>';
    var patt = '/' + str + '/';
    var t = '';
    for (var i = 0; i < (len - 1); i++) {
      if (res[i] != '') {
        var type = res[i].split("@xyz@");
        var type_description = type[0].split("@<br>@");
        var lower_string_name = type_description[0].toLowerCase();
        var lower_string_description = type_description[1];
        var lower_string = type[0].toLowerCase();
        var lower_string1 = str.toLowerCase();
        if (lower_string_name.search(lower_string1) >= 0) {
          if (i % 2 == 0) {
            t += '<tr class="odd"><td><a href="add/' + type[1] + '">' + lower_string_name + '</a><br>' + lower_string_description + '</td> </tr>';
          }
          else {
            t += '<tr class="even"><td><a href="add/' + type[1] + '">' + lower_string_name + '</a><br>' + lower_string_description + '</td> </tr>';
          }
        }
      }
    }
    if (t == '') {
      t = '<tr class="even"><td>' + 'No content found having pattern \'' + str + '\'.</td> </tr>';
    }
    html += t + '</tbody>';
    var obj = document.getElementById('refreshed_table');
    obj.outerHTML = obj.outerHTML.replace(obj.innerHTML , html);
  }
  if (str.length < 3) {
    var res = jstring.split("`");
    var len = res.length;
    var html = '<thead><tr><th>Content Type</th></tr></thead><tbody>';
    var patt = '/' + str + '/';
    for (var i = 0; i < (len - 1); i++) {
      if (res[i] != '') {
        var type = res[i].split("@xyz@");
        var type_description = type[0].split("@<br>@");
        var lower_string_name = type_description[0].toLowerCase();
        var lower_string_description = type_description[1];
        var lower_string1 = str.toLowerCase();
        if (i % 2 == 0) {
          html += '<tr class="even"><td><a href="add/' + type[1] + '">' + lower_string_name + '</a><br>' + lower_string_description + '</td> </tr>';
        }
        else {
          html += '<tr class="odd"><td><a href="add/' + type[1] + '">' + lower_string_name + '</a><br>' + lower_string_description + '</td> </tr>';
        }
      }
    }
    html += '</tbody>';
    var obj = document.getElementById('refreshed_table');
    obj.outerHTML = obj.outerHTML.replace(obj.innerHTML , html);
  }
}
